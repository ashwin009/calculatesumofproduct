package com.ashwin.solution;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * 
 * @author ashwin srivastava
 * 
 *	Time Complexity - The MULTIPLY Operation in the version of BigInteger in jdk8 switches between 
 *	the Naive algorithm {O(n^2)}, 
 *	the Karatsuba algorithm [private static final int KARATSUBA_THRESHOLD = 80;] {O(n^1.585)} and
 *	the Toom-Cook algorithm [private static final int TOOM_COOK_THRESHOLD = 240;] {O(n^1.465)}
 *	depending on the size of the input to get optimized performance.
 *
 *
 */
		
		

public class CalculateSumOfProduct_Using_BigInteger {
	
	private static final BigInteger ZERO = BigInteger.ZERO;
	private static final BigInteger ONE = BigInteger.ONE;
	
	public static BigInteger calculateSum(BigInteger n, BigInteger c) {
		
		BigInteger sum = ZERO;
		
		// for loop from 0 to n-1
		for (BigInteger i = ZERO; i.compareTo(n) < 0; i=i.add(ONE)) {
			
			sum = sum.add(findProduct(i, i.subtract(ONE), c));
			
		}
		
		return sum;
	}

	private static BigInteger findProduct(BigInteger i, BigInteger j, BigInteger c) {
		
		// j is less than ZERO
		if (j.compareTo(ZERO) < 0) return ZERO;
		
		// j is eventually ZERO
		if (j.compareTo(ZERO) == 0) return ONE;
		
		// diff b/w i & j is less than equal to c
		if ((i.subtract(j).compareTo(c)) <= 0) {
			
			// recursive call to findProduct
			return j.add(ONE).multiply(findProduct(i, j.subtract(ONE), c));
			
		}else {
			
			return ONE;
			
		}
	}

}
