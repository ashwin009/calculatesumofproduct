package com.ashwin.solution;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * 
 * @author ashwin srivastava
 * 
 * Time Complexity - O(n^1.585)
 * 
 */
		
		

public class CalculateSumOfProduct_Using_KaratsubaAlgorithm {
	
	private static final BigInteger ZERO = BigInteger.ZERO;
	private static final BigInteger ONE = BigInteger.ONE;
	
	public static BigInteger calculateSum(BigInteger n, BigInteger c) {
		
		BigInteger sum = ZERO;
		
		// for loop from 0 to n-1
		for (BigInteger i = ZERO; i.compareTo(n) < 0; i=i.add(ONE)) {
			
			sum = sum.add(findProduct(i, i.subtract(ONE), c));
			
		}
		return sum;
	}

	private static BigInteger findProduct(BigInteger i, BigInteger j, BigInteger c) {
		
		// j is less ZERO
		if (j.compareTo(ZERO) < 0) return ZERO;
		
		// j is eventually ZERO
		if (j.compareTo(ZERO) == 0) return ONE;
		
		// diff b/w i & j is less than equal to c
		if ((i.subtract(j).compareTo(c)) <= 0) {
			
			// recursive call to findProduct
			return karatsuba(j.add(ONE), findProduct(i, j.subtract(ONE), c));
			
		}else
			
			return ONE;
	}
	
	public static BigInteger karatsuba(BigInteger x, BigInteger y) {

        int N = Math.max(x.bitLength(), y.bitLength());
        System.out.println(N);
        if (N <= 2000) return x.multiply(y);                

        N = (N / 2) + (N % 2);

        // x = a + 2^N b,   y = c + 2^N d
        BigInteger b = x.shiftRight(N);
        BigInteger a = x.subtract(b.shiftLeft(N));
        BigInteger d = y.shiftRight(N);
        BigInteger c = y.subtract(d.shiftLeft(N));

        // compute sub-expressions
        BigInteger ac    = karatsuba(a, c);
        BigInteger bd    = karatsuba(b, d);
        BigInteger abcd  = karatsuba(a.add(b), c.add(d));

        return ac.add(abcd.subtract(ac).subtract(bd).shiftLeft(N)).add(bd.shiftLeft(2*N));
    }


}
